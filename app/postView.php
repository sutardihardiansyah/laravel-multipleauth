<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class postView extends Model
{
    protected $table = 'post_view';

    protected $guarded = ['id'];
}
