<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\postView;

class PostViewController extends Controller
{
    public function index($id)
    {
        postView::create([
            'post_id' => $id
        ]);
    }
}
