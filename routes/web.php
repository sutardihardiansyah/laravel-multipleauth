<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/post-view/{id}', 'PostViewController@index');
// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['namespace' => 'FrontEnd'], function () {
    Route::get('/', 'PostController@index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
